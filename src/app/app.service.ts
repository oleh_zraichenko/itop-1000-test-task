import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

  convert = async(fromCurrency: string, toCurrency: string, amount: number) => 
    this.http.get(`http://api.exchangeratesapi.io/convert?access_key=4090f880e399171ba9755314bb54ddba
    &from=${fromCurrency}&to=${toCurrency}&amount=${amount}`)

}
