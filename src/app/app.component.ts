import { AppService } from './app.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  //url and apikey can be canged
  constructor(private appService: AppService) {}
  title = 'Currency converter';


  // convert = async(currency: string) => await
  //   fetch(this.public_url+ '&from='+currency +'&to=UAH&amount=1').then(response => response.json()).then(data => data.result).catch(error => console.error(error))
  usdCost = this.appService.convert('USD', 'UAH', 1)
  eurCost = this.appService.convert('EUR', 'UAH', 2)

  
}